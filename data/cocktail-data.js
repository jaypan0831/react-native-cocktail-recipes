import Cocktails from '../models/cocktails';

const COCKTAILS = [
  new Cocktails('c1', 'Gin Tonic', require('../assets/images/cocktail.jpg')),
  new Cocktails('c2', 'Long Island Iced Tea', require('../assets/images/cocktail.jpg')),
  new Cocktails('c3', 'Gin Fizz', require('../assets/images/cocktail.jpg')),
  new Cocktails('c4', 'Vesper', require('../assets/images/cocktail.jpg')),
];

export default COCKTAILS;
