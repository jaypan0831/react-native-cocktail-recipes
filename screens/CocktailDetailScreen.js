import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function CocktaileDetailScreen() {
  return (
    <View style={styles.screen}>
      <Text>Details Screen</Text>
    </View>
  );
}

export default CocktaileDetailScreen;
