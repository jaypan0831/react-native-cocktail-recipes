import React from 'react';
import {
  View, Text, StyleSheet, Button,
} from 'react-native';

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function FavoriteScreen({ navigation }) {
  return (
    <View style={styles.screen}>
      <Text>Favorite Screen</Text>
      <Button title='Go to Detail page' onPress={() => navigation.navigate('Detail')} />
    </View>
  );
}

export default FavoriteScreen;
