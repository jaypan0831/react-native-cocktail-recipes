import React from 'react';
import {
  View, Text, StyleSheet, FlatList, Dimensions,
} from 'react-native';
import Image from 'react-native-scalable-image';

import COCKTAILS from '../data/cocktail-data';

const styles = StyleSheet.create({
  screen: {
    margin: 5,
  },
  gridItem: {
    flex: 1,
    justifyContent: 'flex-start',
    margin: 5,
    backgroundColor: '#fff',
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.05,
    shadowRadius: 5,
    elevation: 5,
  },
  imgContainer: {
    overflow: 'hidden',
  },
  title: {
    padding: 20,
  },
});

const renderGridItem = (itemData) => (
  <View style={styles.gridItem}>
    <View style={styles.imgContainer}>
      <Image
        width={Dimensions.get('window').width / 2}
        source={itemData.item.image}
      />
    </View>
    <Text style={styles.title}>{itemData.item.title}</Text>
  </View>
);

const HomeScreen = () => (
  <View style={styles.screen}>
    <FlatList data={COCKTAILS} renderItem={renderGridItem} numColumns={2} />
  </View>
);

export default HomeScreen;
