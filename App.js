import React from 'react';

import TabNavigator from './navigations/TabNavigator';

function App() {
  return (
    <TabNavigator />
  );
}

export default App;
