import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import FavoriteScreen from '../screens/FavoritesScreen';
import CocktailDetailsScreen from '../screens/CocktailDetailScreen';

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name='Favorite' component={FavoriteScreen} />
      <HomeStack.Screen name='Detail' component={CocktailDetailsScreen} />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
