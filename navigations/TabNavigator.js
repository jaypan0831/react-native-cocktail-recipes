import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons } from '@expo/vector-icons';

import HomeStackScreen from './HomeStackNavigator';
import FavoriteStackNavigator from './FavoriteStackNavigator';

const Tab = createBottomTabNavigator();

function TabNavigator() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            switch (route.name) {
              case 'Home':
                iconName = focused ? 'ios-home' : 'md-home';
                break;
              case 'Favorite':
                iconName = focused ? 'ios-heart-empty' : 'ios-heart';
                break;
              default:
                break;
            }

            return <Ionicons name={iconName} size={size} color={color} />;
          },
        })}
        tabBarOptions={{
          activeTintColor: 'tomato',
          inactiveTintColor: 'gray',
        }}
      >
        <Tab.Screen name='Home' component={HomeStackScreen} />
        <Tab.Screen name='Favorite' component={FavoriteStackNavigator} />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

export default TabNavigator;
