import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../screens/HomeScreen';
import CocktailDetailsScreen from '../screens/CocktailDetailScreen';

const HomeStack = createStackNavigator();

function HomeStackScreen() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen name='Home' component={HomeScreen} />
      <HomeStack.Screen name='Detail' component={CocktailDetailsScreen} />
    </HomeStack.Navigator>
  );
}

export default HomeStackScreen;
